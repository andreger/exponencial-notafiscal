var gulp = require('gulp'),
    clean = require('gulp-clean')

gulp.task('clean', function() {
    return gulp.src('dist',  {allowEmpty: true})
        .pipe(clean());
});

gulp.task('copy', function() {
    return gulp.src('src/**/*',  {allowEmpty: true, dot: true, buffer:false})
        .pipe(gulp.dest('dist'));
});

gulp.task('default', gulp.series('clean', 'copy'));

