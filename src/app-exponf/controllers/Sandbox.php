<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sandbox extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('notafiscal_model');
	}
	
	public function teste()
	{
		$data = "File generated: " . date('Y-m-d H:i:s');
		
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/notafiscal/uploads/test_cron.txt', $data);
	}
	

	public function client()
	{
		echo "Testando...";
		soap_consultarNfse();
	}
}
