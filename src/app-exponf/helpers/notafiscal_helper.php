<?php
function get_status($status_id)
{
	switch ($status_id) {
		case STATUS_NOTA_CADASTRADA: return 'Cadastrada';
		case STATUS_NOTA_COM_ERRO: return 'Com Erro';
		case STATUS_NOTA_ENVIADA: return 'Enviada';
		case STATUS_NOTA_GERADA: return 'Gerada';
		case STATUS_NOTA_EMISSAO_CANCELADA: return 'Emissão Cancelada';
        case STATUS_NOTA_IGNORADA: return 'Ignorada';
	}
}

function config_paginacao()
{
	return $config = array(
		"base_url" => base_url('main/listar_notas_fiscais'),
		"per_page" => 50,
		"num_links" => 3,
		"uri_segment" => 3,
		"total_rows" => 0,
		"full_tag_open" => "<ul class='ml-auto bbp-pagination-links' style='display:inline-flex; margin-top: 0'>",
		"full_tag_close" => "</ul>",
		"first_link" => FALSE,
		"last_link" => FALSE,	
		"prev_link" => "&laquo; Anterior",
		"prev_tag_open" => "<li class='t-d-none text-blue page-numbers'>",
		"prev_tag_close" => "</li>",
		"next_link" => "Próximo &raquo;",
		"next_tag_open" => "<li class='t-d-none text-blue page-numbers'>",
		"next_tag_close" => "</li>",
		"last_tag_open" => "<li>",
		"last_tag_close" => "</li>",
		"cur_tag_open" => "<li class='t-d-none page-numbers'><span class='current'>",
		"cur_tag_close" => "</span></li>",
		"num_tag_open" => "<li class='t-d-none text-blue page-numbers'>",
		"num_tag_close" => "</li>"
	);
} 