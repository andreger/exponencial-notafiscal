<?php
function get_listar_notas_fiscais_view_url()
{
	return 'main/listar_notas_fiscais';
}

function get_listar_notas_fiscais_url()
{
	return base_url(get_listar_notas_fiscais_view_url());
}

function get_notacarioca_url($nota)
{
	if($nota['nf_status'] == STATUS_NOTA_GERADA) {
		$url = "https://notacarioca.rio.gov.br/nfse.aspx?inscricao=" . INSCRICAO_MUNICIPAL_EXPONENCIAL . "&nf={$nota['nf_num']}&cod=" . remover_tracos($nota['nf_codigo_verificacao']); 
	}

	return $url; 
}

function get_notacarioca_link($nota)
{
	if($nota['nf_status'] == STATUS_NOTA_GERADA) {
		$url = get_notacarioca_url($nota);
		return "<a href='$url' target='_blank'>Acessar nota</a>";
	}
}

function notacarioca_link($nota)
{
	echo get_notacarioca_link($nota);
}

function get_excluir_nota_fiscal_url($nota_id)
{
	if(($nota['nf_status'] == STATUS_NOTA_CADASTRADA) || ($nota['nf_status'] == STATUS_NOTA_COM_ERRO))  {
		return base_url('main/excluir_nota_fiscal/' . $nota_id);
	}
}

function get_excluir_nota_fiscal_link($nota_id)
{
	if(($nota['nf_status'] == STATUS_NOTA_CADASTRADA) || ($nota['nf_status'] == STATUS_NOTA_COM_ERRO))  {
		$url = get_excluir_nota_fiscal_url($nota_id);
		return "<a href='$url' onclick=\"return confirm('Você realmente deseja apagar este item?')\">Excluir nota</a>";
	}
}

function excluir_nota_fiscal_link($nota_id)
{
//	echo get_excluir_nota_fiscal_link($nota_id);
}