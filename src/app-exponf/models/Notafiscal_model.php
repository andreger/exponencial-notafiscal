<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notafiscal_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_by_order_id($order_id)
	{	
		$this->db->where('order_id', $order_id);
		$query = $this->db->get('notas_fiscais');
		
		return $query->row_array();
	}

	public function listar_todas()
	{
		$this->db->order_by('nf_data_emissao', 'desc');
		$query = $this->db->get('notas_fiscais');
		
		return $query->result_array();
	}

	public function filtrar_notas_fiscais($filtros, $per_page = null, $offset = null)
	{
		$sql = $this->construct_sql($filtros, $per_page, $offset);
		$query = $this->db->query($sql);

		return $query->result_array();
	}

	private function construct_sql($filtros, $per_page = NULL, $offset = NULL, $contar = false)
	{
		$where_data_inicio = $filtros['data_inicio'] ? " AND nf_data_emissao >= '{$filtros['data_inicio']} 00:00:00' " : "";
		$where_data_fim = $filtros['data_fim'] ? " AND nf_data_emissao <= '{$filtros['data_fim']} 23:59:59' " : "";
		$where_ped_data_inicio = $filtros['ped_data_inicio'] ? " AND nf_data_pedido >= '{$filtros['ped_data_inicio']} 00:00:00' " : "";
		$where_ped_data_fim = $filtros['ped_data_fim'] ? " AND nf_data_pedido <= '{$filtros['ped_data_fim']} 23:59:59' " : "";
		$where_status = $filtros['status'] ? "AND nf_status IN (" . implode(",", $filtros['status']) .") " : "";
		$where_pedido_id = $filtros['pedido_id'] ? " AND order_id = '{$filtros['pedido_id']}' " : "";
		$where_nome = $filtros['nome'] ? " AND nf_nome LIKE '%{$filtros['nome']}%' " : "";
		$where_cpf = $filtros['cpf'] ? " AND nf_cpf LIKE '%{$filtros['cpf']}%' " : "";
	
		$select = $contar ?  "COUNT(*) AS total" : "*";

        $limit_offset = "ORDER BY nf_data_pedido DESC LIMIT {$per_page} OFFSET {$offset}";
		if($contar || ! $per_page) {
            $limit_offset = '';
        }

		return "SELECT {$select} FROM notas_fiscais 
				WHERE 1 = 1 {$where_data_inicio} {$where_data_fim} {$where_ped_data_inicio} {$where_ped_data_fim} {$where_status} {$where_pedido_id} {$where_nome} {$where_cpf} 
				{$limit_offset}";
		
	}

	public function contar_notas_fiscais($filtros)
	{
		$sql = $this->construct_sql($filtros, "","", true);
		$query = $this->db->query($sql);
		
		$linha = $query->row_array();
		return $linha['total'];
	}

	public function listar_cadastradas($is_para_envio = TRUE)
	{
		$this->db->from('notas_fiscais');
		$this->db->where('nf_status', STATUS_NOTA_CADASTRADA);

		if($is_para_envio){
			$dias= DIAS_PARA_ENVIO_NF;
			$dias_aux = $dias - 1;
			$this->db->where("nf_data_pedido < (CURRENT_DATE - INTERVAL {$dias_aux} DAY)");
			$this->db->where("nf_data_pedido >= (CURRENT_DATE - INTERVAL {$dias} DAY)");
			
			$this->db->limit(20);
		}

		$query = $this->db->get();

		return $query->result_array();
	}
	
	public function listar_cadastradas_acerto()
	{
	    $this->db->from('notas_fiscais');
	    $this->db->where('nf_status', STATUS_NOTA_CADASTRADA);

        $this->db->where("nf_data_pedido <= '2019-06-18 23:59:59'");
        $this->db->where("nf_data_pedido >= '2019-06-01 00:00:00'");
        
        $this->db->limit(10);
 
	    $query = $this->db->get();
	    
	    return $query->result_array();
	}
	
	public function atualizar_codigo_verificacao($nota_id, $codigo)
	{
		$this->db->where('order_id', $nota_id);
		$this->db->set('nf_codigo_verificacao', $codigo);
		$this->db->update('notas_fiscais');
	}
	
	public function atualizar_numero($nota_id, $numero)
	{
		$this->db->where('order_id', $nota_id);
		$this->db->set('nf_num', $numero);
		$this->db->update('notas_fiscais');
	}
	
	public function atualizar_status($nota_id, $status)
	{
		$this->db->where('order_id', $nota_id);
		$this->db->set('nf_status', $status);
		$this->db->update('notas_fiscais');
	}

	public function atualizar_valor_e_emissao($nota_id, $valor, $data_emissao){
		$this->db->where('order_id', $nota_id);
		$this->db->set('nf_valor', $valor);
		$this->db->set('nf_data_emissao', $data_emissao);
		$this->db->update('notas_fiscais');
	}

	public function atualizar_erro($nota_id, $erro_codigo, $erro_mensagem, $erro_correcao)
	{
		$this->db->where('order_id', $nota_id);
		$this->db->set('nf_erro_codigo', $erro_codigo);
		$this->db->set('nf_erro_mensagem', $erro_mensagem);
		$this->db->set('nf_erro_correcao', $erro_correcao);
		$this->db->update('notas_fiscais');
	}
	
	public function excluir($nota_id)
	{
		$this->db->where('order_id', $nota_id);
		$this->db->delete('notas_fiscais');
	}
	
	public function limpar_erro($nota_id)
	{
		self::atualizar_erro($nota_id, '', '', '');
	}
}