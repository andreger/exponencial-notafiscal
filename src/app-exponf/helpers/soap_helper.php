<?php
function get_soap_client() 
{
	$certificado_path = $_SERVER['DOCUMENT_ROOT'] . '/notafiscal/assets/certs/exponencial.pem';

	try{
		$client = new SoapClient(NOTACARIOCA_WSDL,
			array('local_cert' => $certificado_path,
				'trace' => 1,
				'use' => SOAP_LITERAL,
				'exceptions' => true,
				'encoding'=> 'UTF-8',
				'verifypeer' => false,
				'verifyhost' => false,
				'soap_version' => SOAP_1_1
			));
		
		return $client;

	} catch (Throwable $e) {
		print_r($e);
	}
}

function soap_consultarNfse()
{

	$um_mes_atras = date('Y-m-d', strtotime('-3 month'));
	$hoje = date('Y-m-d');
	
	$xml =
		"<ConsultarNfseEnvio xmlns='http://www.abrasf.org.br/ABRASF/arquivos/nfse.xsd'>
	    	<Prestador>
				<Cnpj>".CNPJ_EXPONENCIAL."</Cnpj>
				<InscricaoMunicipal>".INSCRICAO_MUNICIPAL_EXPONENCIAL."</InscricaoMunicipal>
			</Prestador>
			<PeriodoEmissao>
				<DataInicial>{$um_mes_atras}</DataInicial>
				<DataFinal>{$hoje}</DataFinal>
			</PeriodoEmissao>
	    </ConsultarNfseEnvio>";

	try {
		$client = get_soap_client();

		// print_r($xml);

		$result = $client->ConsultarNfse(array('inputXML' => $xml));

		print_r($result);
	}
	catch(Exception $e) {
		print_r($e);
	}
	// print_r($result);
	// var_dump($client->__getLastRequest());
	// print_r($result->outputXML);
	// echo "====== REQUEST HEADERS =====" . PHP_EOL;
 //    var_dump($client->__getLastRequestHeaders());
 //    echo "========= REQUEST ==========" . PHP_EOL;
 //    var_dump($client->__getLastRequest());
 //    echo "========= RESPONSE =========" . PHP_EOL;
 //    var_dump($response);

}

function consultarNfsePorNumero($nf_numero)
{
	$xml =
		"<ConsultarNfseEnvio xmlns='http://www.abrasf.org.br/ABRASF/arquivos/nfse.xsd'>
	    	<Prestador>
				<Cnpj>".CNPJ_EXPONENCIAL."</Cnpj>
				<InscricaoMunicipal>".INSCRICAO_MUNICIPAL_EXPONENCIAL."</InscricaoMunicipal>
			</Prestador>
            <NumeroNfse>{$nf_numero}</NumeroNfse>  
	    </ConsultarNfseEnvio>";
	
	$client = get_soap_client();
	$result = $client->ConsultarNfse(array('inputXML' => $xml));
	
	return $result->outputXML;
}

function soapGerarNfse($dados) 
{
	$data_emissao = formatar_data_para_nota_fiscal($dados['nf_data_emissao']);
	
	$endereco = $dados['nf_endereco'] == null ? '' : $dados['nf_endereco'];
	$numero = $dados['nf_numero'] == null ? '' : $dados['nf_numero'];
	$complemento = $dados['nf_complemento'] == null ? '' : $dados['nf_complemento'];
	$bairro = $dados['nf_bairro'] == null ? '' : $dados['nf_bairro'];
	$uf = $dados['nf_uf'] == null ? '' : $dados['nf_uf'];
	$cep = $dados['nf_cep'] == null ? '' : $dados['nf_cep'];
	$cep = remover_tracos($cep);
	
	$xml = 
		"<GerarNfseEnvio xmlns='http://notacarioca.rio.gov.br/WSNacional/XSD/1/nfse_pcrj_v01.xsd'>
	     	<Rps>
	        	<InfRps xmlns='http://www.abrasf.org.br/ABRASF/arquivos/nfse.xsd' Id='R1'>
	          		<IdentificacaoRps>
	            		<Numero>{$dados['order_id']}</Numero>
	            		<Serie>ABC</Serie>
	            		<Tipo>1</Tipo>
	          		</IdentificacaoRps>
					<DataEmissao>{$data_emissao}</DataEmissao>
					<NaturezaOperacao>1</NaturezaOperacao>
					<OptanteSimplesNacional>1</OptanteSimplesNacional>
					<IncentivadorCultural>2</IncentivadorCultural>
					<Status>1</Status>
					<Servico>
						<Valores>
							  <ValorServicos>{$dados['nf_valor']}</ValorServicos>
							  <IssRetido>2</IssRetido>
						</Valores>
						<ItemListaServico>0802</ItemListaServico>
						<CodigoTributacaoMunicipio>080216</CodigoTributacaoMunicipio>
						<Discriminacao>{$dados['nf_discriminacao']}</Discriminacao>
						<CodigoMunicipio>3304557</CodigoMunicipio>
					</Servico>
					<Prestador>
						<Cnpj>".CNPJ_EXPONENCIAL."</Cnpj>
						<InscricaoMunicipal>".INSCRICAO_MUNICIPAL_EXPONENCIAL."</InscricaoMunicipal>
					</Prestador>
					<Tomador>
						<IdentificacaoTomador>
							<CpfCnpj>
								<Cpf>{$dados['nf_cpf']}</Cpf>
							</CpfCnpj>
						</IdentificacaoTomador>
						<RazaoSocial>{$dados['nf_nome']}</RazaoSocial>
						<Endereco>
							<Endereco>{$endereco}</Endereco>
							<Uf>{$uf}</Uf>
							<Cep>{$cep}</Cep>
						</Endereco>
						<Contato>
							<Email>{$dados['nf_email']}</Email>
						</Contato>
					</Tomador>
		        </InfRps>
		      </Rps>
		</GerarNfseEnvio>";
	
	$client = get_soap_client();

	try {
		$result = $client->GerarNfse(array('inputXML' => $xml));
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e);
	}
	
	return $result->outputXML;
}

