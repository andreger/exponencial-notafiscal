<?php get_header() ?>
<div class="container-fluid">
<div class="pt-4 mb-5 col-12 text-center text-md-left text-blue">
		<h1>Notas Fiscais</h1>
	</div>
</div>
<div class="container">		
<div class="">
	<form method="post" action="/notafiscal/main/listar_notas_fiscais">
	<div class="row frm-filtro-nf mr-0 ml-0" id="filtro">

        <div class="col-2">
            <label class="control-label mt-2 mb-4"><strong>Status:</strong></label>
        </div>
		<div class="col-10">
            <div class="row">
                <div class="col-2">
                    <label class="checkbox mt-2">
                        <?= form_checkbox('status[]', STATUS_NOTA_CADASTRADA, in_array(STATUS_NOTA_CADASTRADA, $form_nf['status'])) ?> Cadastrada
                    </label>
                </div>

                <div class="col-2">
                    <label class="checkbox mt-2">
                        <?= form_checkbox('status[]', STATUS_NOTA_EMISSAO_CANCELADA, in_array(STATUS_NOTA_EMISSAO_CANCELADA, $form_nf['status'])) ?> Em. Cancelada
                    </label>
                </div>

                <div class="col-2">
                    <label class="checkbox mt-2">
                        <?= form_checkbox('status[]', STATUS_NOTA_COM_ERRO, in_array(STATUS_NOTA_COM_ERRO, $form_nf['status'])) ?> Com Erro
                    </label>
                </div>

                <div class="col-2">
                    <label class="checkbox mt-2">
                        <?= form_checkbox('status[]', STATUS_NOTA_ENVIADA, in_array(STATUS_NOTA_ENVIADA, $form_nf['status'])) ?> Enviada
                    </label>
                </div>

                <div class="col-2">
                    <label class="checkbox mt-2">
                        <?= form_checkbox('status[]', STATUS_NOTA_GERADA, in_array(STATUS_NOTA_GERADA, $form_nf['status'])) ?> Gerada
                    </label>
                </div>

                <div class="col-2">
                    <label class="checkbox mt-2">
                        <?= form_checkbox('status[]', STATUS_NOTA_IGNORADA, in_array(STATUS_NOTA_IGNORADA, $form_nf['status'])) ?> Ignorada
                    </label>
                </div>
            </div>
        </div>

		<div class="col-2">
			<label class="control-label mt-2 mb-4"><strong>Data de Emissão:</strong></label>
		</div>

        <div class="col-10">
            <div class="row">
                <div class="col-5">
                    <input class="form-control" type="text" name="start_date" id="start_date" placeholder="Data início" autocomplete="off" value="<?= $form_nf['start_date'] ?>">
                </div>

                <div class="col-1 text-center mt-2">até</div>

                <div class="col-5">
                    <input class="form-control" type="text" name="end_date" id="end_date" placeholder="Data fim" autocomplete="off" value="<?= $form_nf['end_date'] ?>">
                </div>

                <div class="col-1"></div>
            </div>
        </div>

		<div class="col-2">
			<label class="control-label mt-2 mb-4"><strong>Data do Pedido:</strong></label>
		</div>

		<div class="col-10">
            <div class="row">
                <div class="col-5">
   			        <input class="form-control" type="text" name="ped_start_date" id="ped_start_date" placeholder="Data início" autocomplete="off" value="<?= $form_nf['ped_start_date'] ?>">
   		        </div>

                <div class="col-1 text-center mt-2">até</div>

                <div class="col-5">
                    <input class="form-control" type="text" name="ped_end_date" id="ped_end_date" placeholder="Data fim" autocomplete="off" value="<?= $form_nf['ped_end_date'] ?>">
                </div>

                <div class="col-1"></div>
            </div>
        </div>

		<div class="col-2">
			<label class="control-label mt-2 mb-4"><strong>Pedido:</strong></label>
		</div>

		<div class="col-10">
   			<input class="form-control" type="text" name="pedido_id" id="pedido_id" placeholder="" autocomplete="off" value="<?= $form_nf['pedido_id'] ?>">
   		</div>

   		<div class="col-2">
			<label class="control-label mt-2 mb-4"><strong>Nome:</strong></label>
		</div>

		<div class="col-10">
   			<input class="form-control" type="text" name="nome" id="nome" placeholder="" autocomplete="off" value="<?= $form_nf['nome'] ?>">
   		</div>

   		<div class="col-2">
			<label class="control-label mt-2 mb-4"><strong>CPF:</strong></label>
		</div>

		<div class="col-10">
   			<input class="form-control" type="text" name="cpf" id="cpf" placeholder="" autocomplete="off" value="<?= $form_nf['cpf'] ?>">
   		</div>

		<div class="col-12">
			 <button class="btn u-btn-blue" type="submit" id="filtrar-btn" name="filtrar" value="1"><i class="fa fa-filter"></i> Filtrar</button>
             <a href="/notafiscal/main/exportar_notas_fiscais" class="btn u-btn-blue"><i class="fa fa-filter"></i> Exportar</a>
			 <button class="btn btn-white" id="limpar-filtros-btn"><i class="fa fa-eraser"></i> Limpar Filtros</button>
		</div>
	</div>
	</form>
	<div class="row" style="margin-top: 30px">
		<div class='col-6'>
			<div class="mt-2">
			Registros encontrados: <strong><?= $total ?></strong>

			<?php if($tem_filtro_aplicado) :?>
				<i>(Filtro aplicado)</i>
			<?php endif ?>
			</div>
		</div>

		<div class="col-6">
			<?= $pagination; ?>
		</div>
	</div>

	<?php if($notas) : ?>

	<table style='margin:10px 0;' border='0' cellspacing='0' cellpadding='0' class='table table-bordered table-striped'>
		<tr class="text-center font-10 bg-blue text-white">
			<!--<th>Número da Nota</th>-->
			<th>Pedido / RPS</th>
			<th>Data do Pedido</th>
			<th>Nome</th>
			<th>CPF</th>
			<th>Valor da Nota</th>
			<th>Data de Emissão</th>
			<th>Código de Verificação</th>
			<th>Status</th>
			<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ações&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		</tr>
	
		<?php foreach($notas as $nota) : ?>
		<tr class="text-center">
			<!--<td><?= $nota['nf_num'] ?></td>-->
			<td><?= $nota['order_id'] ?></td>
			<td><?= formatar_data_para_interface($nota['nf_data_pedido']) ?></td>
			<td class="text-left"><?= $nota['nf_nome'] ?></td>
			<td><?= $nota['nf_cpf'] ?></td>
			<td><?= $nota['nf_valor'] ? moeda($nota['nf_valor'], false) : '' ?></td>
			<td><?= $nota['nf_data_emissao'] ? formatar_data_para_interface($nota['nf_data_emissao']) : '' ?></td>
			<td><?= $nota['nf_codigo_verificacao'] ?></td>
			<td><?= get_status($nota['nf_status']) ?></td>
			<td><?php notacarioca_link($nota) ?> <?php excluir_nota_fiscal_link($nota['nf_num']) ?></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php endif ?>

	<div>
		<?= $pagination; ?>
	</div>

	<div style="margin:30px 0">
		<div>Informações sobre status:</div>
		<div><strong>Cadastrada</strong> - Dados da NF foram cadastrados, mas ainda não foi enviada para prefeitura.</div>
		<div><strong>Emissão Cancelada</strong> - Dados da NF não foram enviados devido ao cancelamento ou estorno total do pedido.</div>
		<div><strong>Com Erro</strong> - Tentativa de envio de RPS com falha. Entrar em contato com administrador do sistema.</div>
		<div><strong>Enviada</strong> - RPS enviado, mas ainda não processado pelo sistema da prefeitura (assíncrono).</div>
		<div><strong>Gerada</strong> - RPS enviado e Código de Verificação obtido. NF pode ser acessada pelo link.</div>
        <div><strong>Ignorada</strong> - NF não foi cadastrada para emissão.</div>
	</div>
</div>
</div>
<script src='/questoes/assets-admin/js/jquery-2.1.1.js'></script>

<script> 
	$(document).ready(function(){
		$("#start_date").datepicker();  
	    $("#end_date").datepicker();
		$("#ped_start_date").datepicker();  
	    $("#ped_end_date").datepicker();
	    $.datepicker.setDefaults({  
	        dateFormat: 'dd/mm/yy',
	        dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
	        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
	        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	        nextText: 'Próximo',
	        prevText: 'Anterior' 
	    });  

	  	$("#limpar-filtros-btn").click(function(e) {
	  		e.preventDefault();
	  		$("#filtro input[type='text']").prop("value", "");
	  		$("#filtro input[type='checkbox']").prop("checked",false);
	  		$("#filtrar-btn").click();
	  	});

	  	$(".frm-filtro-nf input[type='text']").keypress(function(e) {
		    if(e.which == 13) {
		        e.preventDefault();
		        $('#filtrar-btn').click();
		    }
		});

	}); 

</script>
<?php get_footer() ?>