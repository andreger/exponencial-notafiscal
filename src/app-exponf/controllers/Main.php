<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

//require_once APPPATH. '/third_party/spout-master/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class Main extends CI_Controller {

	public function __construct()
	{
		session_start();

		parent::__construct();

		$this->load->model('notafiscal_model');
		
		$this->load->helper('notafiscal');
		$this->load->helper('form');
		
		$this->load->library('pagination');
	}
	
	public function index()
	{
		redirect(get_listar_notas_fiscais_url());
	}
	
	public function listar_notas_fiscais()
	{
		// Controle de permissões
		redirecionar_se_nao_for_administrador();

		KLoader::helper("FiltroHelper");		

		// Inicializa a sessão
		if(!$_SESSION['form_nf']) {

			$_SESSION['form_nf'] = [
				'start_date' => null,
				'end_date' => null,
				'ped_start_date' => null,
				'ped_end_date' => null,
				'status' => null,
				'pedido_id' => null,
				'nome' => null,
				'cpf' => null
			];
		}

		// Alimenta a sessão com dados do formulário
		if($this->input->post('filtrar')) 
		{	
			$_SESSION['form_nf'] = [
				'start_date' => trim($this->input->post('start_date')) ?: null,
				'end_date' => trim($this->input->post('end_date')) ?: null,
				'ped_start_date' => trim($this->input->post('ped_start_date')) ?: null,
				'ped_end_date' =>trim($this->input->post('ped_end_date')) ?: null,
				'status' => $this->input->post('status') ?: null,
				'pedido_id' => trim($this->input->post('pedido_id')) ?: null,
				'nome' => trim($this->input->post('nome')) ?: null,
				'cpf' => trim($this->input->post('cpf')) ?: null
			];
		}		

		// Monta dados para chamadas de modelo
		$filtros = [
			"data_inicio" => converter_para_yyyymmdd($_SESSION['form_nf']['start_date']),
			"data_fim" => converter_para_yyyymmdd($_SESSION['form_nf']['end_date']),
			"ped_data_inicio" => converter_para_yyyymmdd($_SESSION['form_nf']['ped_start_date']),
			"ped_data_fim" => converter_para_yyyymmdd($_SESSION['form_nf']['ped_end_date']),
			"status" => $_SESSION['form_nf']['status'],
			"pedido_id" => $_SESSION['form_nf']['pedido_id'],
			"nome" => $_SESSION['form_nf']['nome'],
			"cpf" => $_SESSION['form_nf']['cpf'],
		];

		// Montagem da paginação
		$config = config_paginacao();	
		$offset = $this->uri->segment(3) ?: 0;		
		$config['total_rows'] = $this->notafiscal_model->contar_notas_fiscais($filtros);
		$data['total'] = numero_inteiro($config['total_rows']);

		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		// Execução da consulta
		$notas = $this->notafiscal_model->filtrar_notas_fiscais($filtros, $config['per_page'], $offset);		
		$data['notas'] = $notas;
		$data['form_nf'] = $_SESSION['form_nf'];
		$data['tem_filtro_aplicado'] = FiltroHelper::tem_filtro_aplicado($_SESSION['form_nf']);

		$this->load->view(get_listar_notas_fiscais_view_url(), $data);
	}

	public function exportar_notas_fiscais()
    {
        $filtros = [
            "data_inicio" => converter_para_yyyymmdd($_SESSION['form_nf']['start_date']),
            "data_fim" => converter_para_yyyymmdd($_SESSION['form_nf']['end_date']),
            "ped_data_inicio" => converter_para_yyyymmdd($_SESSION['form_nf']['ped_start_date']),
            "ped_data_fim" => converter_para_yyyymmdd($_SESSION['form_nf']['ped_end_date']),
            "status" => $_SESSION['form_nf']['status'],
            "pedido_id" => $_SESSION['form_nf']['pedido_id'],
            "nome" => $_SESSION['form_nf']['nome'],
            "cpf" => $_SESSION['form_nf']['cpf'],
        ];

        $notas = $this->notafiscal_model->filtrar_notas_fiscais($filtros);

        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser("notas-fiscais.xlsx");
        $writer->addRow([
            'Pedido / RPS', 'Data do Pedido', 'Nome', 'CPF', 'Valor da Nota', 'Data de Emissão', 'Código de Verificação', 'Status'
        ]);

        foreach ($notas as $nota) {
            $writer->addRow([
                $nota['order_id'],
                formatar_data_para_interface($nota['nf_data_pedido']),
                $nota['nf_nome'],
                $nota['nf_cpf'],
                $nota['nf_valor'] ? moeda($nota['nf_valor'], false) : '',
                $nota['nf_data_emissao'] ? formatar_data_para_interface($nota['nf_data_emissao']) : '',
                $nota['nf_codigo_verificacao'],
                get_status($nota['nf_status']),

            ]);
        }

        $writer->close();
    }
	
	public function excluir_nota_fiscal($nota_id)
	{
		$this->notafiscal_model->excluir($nota_id);
		
		redirect(get_listar_notas_fiscais_url());
	}
	
	
	public function enviar_notas()
	{
		$notas = $this->notafiscal_model->listar_cadastradas(TRUE);

		echo count($notas) . " nota(s) cadastrada(s)<br>";
		
		foreach ($notas as $nota) {
			self::processar_nota($nota);
		}
	}
	
	
	public function enviar_notas_acerto()
	{
	    $notas = $this->notafiscal_model->listar_cadastradas_acerto();
	   
	    echo "<html><head><meta http-equiv='refresh' content='5'></head><body>";

	    echo count($notas) . " nota(s) cadastrada(s)<br>";
	    
	    foreach ($notas as $nota) {
	        self::processar_nota($nota);
	    }
	    
	    echo "</body></html>";
	}
	
	public function listar_ultimas()
	{
		soap_consultarNfse();
	}

    public function enviar_nota($id)
    {
        $nota = $this->notafiscal_model->get_by_order_id($id);
        self::processar_nota($nota);
    }

	private function processar_nota($nota)
	{
		echo "Processando nota:" . $nota['order_id'] . "<br>";			

		//Verifica se o pedido possui valor e precisa ser enviado
		$order = new WC_Order($nota['order_id']);
		$total = $order->get_total() + $order->get_total_discount();

		$is_cancelled = $order->status == 'cancelled' 
						|| ($order->status == 'refunded' && $order->get_remaining_refund_amount() <= 0)
						|| $total <= 0;

		if($is_cancelled){
			
			echo "Nota sem valor, emissão cancelada<br>";
			$this->notafiscal_model->atualizar_status($nota['order_id'], STATUS_NOTA_EMISSAO_CANCELADA);

		}else{

			//Salva o valor e data de emissão da NF
			$nf_valor = $order->get_remaining_refund_amount();
			$nf_data_emissao = date('Y-m-d H:i:s');
			$this->notafiscal_model->atualizar_valor_e_emissao($nota['order_id'], $nf_valor, $nf_data_emissao);

			$nota['nf_valor'] = $nf_valor;
			$nota['nf_data_emissao'] = $nf_data_emissao;

			$xml_retorno = soapGerarNfse($nota);

			echo "Retorno<br>";

			$sx = simplexml_load_string($xml_retorno);

			echo "SX<br>";
			
			if(isset($sx->ListaMensagemRetorno->MensagemRetorno)) {
				echo "Nota com erro<br>";

				$erro_codigo = (String)$sx->ListaMensagemRetorno->MensagemRetorno->Codigo;
				$erro_mensagem = (String)$sx->ListaMensagemRetorno->MensagemRetorno->Mensagem;
				$erro_correcao = (String)$sx->ListaMensagemRetorno->MensagemRetorno->Correcao;
			
				$this->notafiscal_model->atualizar_erro($nota['order_id'], $erro_codigo, $erro_mensagem, $erro_correcao);
				$this->notafiscal_model->atualizar_status($nota['order_id'], STATUS_NOTA_COM_ERRO);
			
			}
			elseif(isset($sx->CompNfse->Nfse->InfNfse->CodigoVerificacao)) {
				echo "Nota gerada<br>";

				$codigo = (String)$sx->CompNfse->Nfse->InfNfse->CodigoVerificacao;
				$numero = (String)$sx->CompNfse->Nfse->InfNfse->Numero;
				
				$this->notafiscal_model->limpar_erro($nota['order_id']);
				$this->notafiscal_model->atualizar_status($nota['order_id'], STATUS_NOTA_GERADA);
				$this->notafiscal_model->atualizar_codigo_verificacao($nota['order_id'], $codigo);
				$this->notafiscal_model->atualizar_numero($nota['order_id'], $numero);
			}
			else {
				echo "Nota Enviada<br>";

				$this->notafiscal_model->limpar_erro($nota['order_id']);
				$this->notafiscal_model->atualizar_status($nota['order_id'], STATUS_NOTA_ENVIADA);
				
				enviar_email($nota['nf_email']);
			}
		}

	}
	
	public function enviar_email($nota)
	{
		$this->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		
		$mensagem = get_notacarioca_url($nota);
		
		$this->email->initialize($config);
		
		$this->email->from('contato@exponencialconcursos.com.br', 'Exponencial Concursos');
		$this->email->to($nota['nf_email']);
// 		$this->email->bcc(array('leonardo.coelho@exponencialconcursos.com.br','andreger@gmail.com'));
		$this->email->bcc(array('andreger@gmail.com'));
		
		$this->email->subject('Nota Fiscal - Exponencial Concursos');
		$this->email->message($mensagem);
		
		$this->email->send();
		
	}
	
	public function emitir_notas()
	{
		
	}
}
