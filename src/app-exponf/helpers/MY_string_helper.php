<?php
function remover_tracos($str)
{
	return str_replace('-', '', $str);	
}

function formatar_data_para_nota_fiscal($str)
{
	return str_replace(' ', 'T', $str);
}

function formatar_data_para_interface($str)
{
	return date('d/m/Y H:i:s', strtotime($str));
}
